const { expect } = require('chai');
const fetchPlayerStats = require('../lib/index.js');
const validate = require('jsonschema').validate;

describe('tastyspleen-scraper', () => {
  it('should adhere to the following json schema', async () => {
    const jsonResults = await fetchPlayerStats('Jimmy');
    const validSchema = {
      type : 'object',
      properties  : {
        'alltime_frags_by_server' : 
          { 'type' : 'object' },
        'alltime_deaths_by_server' :
          { 'type' : 'object' },
        'alltime_frags_by_weapon' : 
          { 'type' : 'object' },
        'alltime_deaths_by_weapon' :
          { 'type' : 'object' },
      },
      required: ['alltime_frags_by_server', 'alltime_deaths_by_server', 'alltime_frags_by_weapon', 'alltime_deaths_by_weapon']
    };

    expect(validate(jsonResults, validSchema).valid).to.be.true;
  });

});
