const axios = require('axios');
const cheerio = require('cheerio');
const { parse, each } = require('abstract-syntax-tree');

// Define object key map
const keyMap = {
  draw_pie_frags_alltime_by_server: 'alltime_frags_by_server',
  draw_pie_deaths_alltime_by_server: 'alltime_deaths_by_server',
  draw_pie_frags_alltime_by_weapon: 'alltime_frags_by_weapon',
  draw_pie_deaths_alltime_by_weapon: 'alltime_deaths_by_weapon',
};

// Merge two object arrays into an object of the following structure
//  {
//   "<key1>" : "<value1>",
//       ...
//   "<keyN>" : "<valueN>"
//   }
function mergeArrays(keys, values) {
  const obj = {};

  keys.forEach((key, i) => {
    obj[key.value] = values[i].value;
  });

  return obj;
}

// Define the function that extracts a given player's stats
const fetchPlayerStats = async function fetchPlayerStats(playerName) {
  const url = `http://db.tastyspleen.net/cgi-bin/top-frags.cgi?name=${playerName}&frame=detail_frame`;
  const targetFunctions = ['draw_pie_frags_alltime_by_server', 'draw_pie_deaths_alltime_by_server', 'draw_pie_frags_alltime_by_weapon', 'draw_pie_deaths_alltime_by_weapon'];

  const { data } = await axios.get(url);

  // load the page source with cheerio to query the elements
  const $ = cheerio.load(data);

  // get the script tag that contains the string 'Chart.defaults'
  const scriptContents = $('script')
    .toArray()
    .map((script) => $(script).html())
    .find((contents) => contents.includes('init'));

  // Obtain the abstract-syntax-tree
  const ast = parse(scriptContents);

  // Define variable that represent the object we wish to return
  const result = {};

  // Iterate over all function declarations to find the four functions that contain the target data
  each(ast, 'FunctionDeclaration', (node) => {
    // If the FunctionDeclaration is one of the target functions
    if (targetFunctions.includes(node.id.name)) {
      // label elements
      const keys = node.body.body[1].declarations[0].init.elements;

      // values elements
      const values = node.body.body[2].declarations[0].init.elements;

      // Merge the two arrays
      const mergedArrays = mergeArrays(keys, values);

      // Reorganize the array into an object with the following structure
      // {
      //  "type" : "<function name>"
      //  "values" : {
      //     "<element1>" : "<value1>",
      //         ...
      //     "<elementN>" : "<valueN>"
      //   }
      // }

      // Replace object keys
      const type = node.id.name.replace(node.id.name, (matched) => keyMap[matched]);

      // Create object element
      result[type] = mergedArrays;
    }
  });

  // return results
  return result;
};

module.exports = fetchPlayerStats;
