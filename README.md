# tastyspleen-stats-scraper

> Scrapes Quake2 player stats from http://tastyspleen.net.

## Installation

```sh
$ npm install --save tastyspleen-stats-scraper
```

## Usage

```js
const tsScraper = require('tastyspleen-stats-scraper');

tsScraper ('NoobMurker');
```

### Results: 

```js
{
   "alltime_frags_by_server":
      {
         "dm":6772,
         "railz":410,
         "ffa":78,
         "ts500":5,
         "mutant":3,
         "unrailz":2,
         "tourney":1
      },
   "alltime_deaths_by_server":
      {
         "dm":6193,
         "railz":347,
         "ffa":79,
         "mutant":12,
         "unrailz":2,
         "tourney":2,
         "vanilla":2,
         "mootown":1
      },
   "alltime_frags_by_weapon":
      {
         "rl":2277,
         "rg":2070,
         "gl":832,
         "ssg":486,
         "bfg":460,
         "grenade":418,
         "hb":187,
         "mg":179,
         "cg":168,
         "blaster":93,
         "sg":78,
         "telefrag":23
      },
   "alltime_deaths_by_weapon":
      {
         "rl":2026,
         "rg":1812,
         "gl":785,
         "ssg":716,
         "bfg":301,
         "grenade":275,
         "cg":220,
         "hb":204,
         "mg":144,
         "blaster":71,
         "sg":62,
         "telefrag":22
      }
}
```

## License

Apache-2.0 © [Andrew Gagnon]()
